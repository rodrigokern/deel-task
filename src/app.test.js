const supertest = require('supertest')
const app = require('./app')
const request = supertest(app)

describe('GET /contracts/1', () => {
    it('responds with contract 1', async () => {
        const response = await request
            .get('/contracts/1')
            .set('profile_id', '1')

        expect(response.statusCode).toEqual(200)

        expect(response.body.id).toEqual(1)
        expect(response.body.ContractorId).toEqual(5)
        expect(response.body.ClientId).toEqual(1)
    })
})

describe('GET /contracts', () => {
    it('lists non terminated contracts', async () => {
        const response = await request
            .get('/contracts')
            .set('profile_id', '1')

        expect(response.statusCode).toEqual(200)

        expect(response.body.length).toEqual(1)
    })
})

describe('GET /jobs/unpaid', () => {
    it('lists non terminated contracts', async () => {
        const response = await request
            .get('/jobs/unpaid')
            .set('profile_id', '1')

        expect(response.statusCode).toEqual(200)

        expect(response.body.length).toEqual(2)
        expect(response.body[0].paid).not.toBe(true)
        expect(response.body[1].paid).not.toBe(true)
    })
})
