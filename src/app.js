const express = require('express');
const bodyParser = require('body-parser');
const { sequelize } = require('./model')
const { Op } = require('sequelize')
const { getProfile } = require('./middleware/getProfile')
const app = express();
app.use(bodyParser.json());
app.set('sequelize', sequelize)
app.set('models', sequelize.models)

/**
 * @returns contract by id
 */
app.get('/contracts/:id', getProfile, async (req, res) => {
    const { Contract } = req.app.get('models')
    const { id } = req.params
    const contract = await Contract.findOne({
        where: {
            [Op.and]: [
                { id },
                {
                    [Op.or]: [
                        { ClientId: req.profile.id },
                        { ContractorId: req.profile.id }
                    ]
                }
            ]
        }
    })

    if (!contract) return res.status(404).end()
    res.json(contract)
})

/**
 * @returns lists non terminated contracts
 */
app.get('/contracts', getProfile, async (req, res) => {
    const { Contract } = req.app.get('models')
    const contracts = await Contract.findAll({
        where: {
            [Op.and]: [
                { status: { [Op.ne]: 'terminated' } },
                {
                    [Op.or]: [
                        { ClientId: req.profile.id },
                        { ContractorId: req.profile.id }
                    ]
                }
            ]
        }
    })

    if (!contracts) return res.status(404).end()
    res.json(contracts)
})

/**
 * @returns lists unpaid jobs for a user (active contracts only)
 */
app.get('/jobs/unpaid', getProfile, async (req, res) => {
    const { Contract, Job } = req.app.get('models')
    const jobs = await Job.findAll({
        include: {
            model: Contract,
            where: { ClientId: req.profile.id },
            attributes: []
        },
        where: {
            paid: { [Op.eq]: null }
        }
    })

    if (!jobs) return res.status(404).end()
    res.json(jobs)
})

/**
 * Pay for a job
 */
app.post('/jobs/:job_id/pay', getProfile, async (req, res) => {
    if (req.profile.type != 'client') return res.status(404).end()
    const { Contract, Job, Profile } = req.app.get('models')
    const { job_id } = req.params
    const contract = await Contract.findOne({
        include: {
            model: Job,
            where: {
                id: job_id,
                paid: { [Op.eq]: null }
            }
        },
        where: { ClientId: req.profile.id }
    })

    if (!contract) return res.status(404).end()

    const job = contract.Jobs[0]
    const { price } = job

    if (price > req.profile.balance) return res.status(400).end()

    try {

        await sequelize.transaction(async (transaction) => {

            Profile.decrement('balance', { by: price, where: { id: contract.ClientId } }, { transaction })
            Profile.increment('balance', { by: price, where: { id: contract.ContractorId } }, { transaction })

            job.set({
                paid: true,
                paymentDate: new Date()
            })
            await job.save({ transaction });
        });

    } catch (error) {
        console.log(error)
        return res.status(500).end()
    }

    res.end()
})

/**
 * Deposits money into the the the balance of a client
 * a client can't deposit more than 25% his total of jobs to pay. (at the deposit moment)
 * Obs: changed param userId to amount
 */
app.post('/balances/deposit/:amount', getProfile, async (req, res) => {
    if (req.profile.type != 'client') return res.status(404).end()
    const { Contract, Job } = req.app.get('models')
    const { amount } = req.params

    try {

        await sequelize.transaction(async (transaction) => {

            const depositLimit = await Job.sum('price', {
                include: {
                    model: Contract,
                    where: { ClientId: req.profile.id },
                },
                where: {
                    paid: { [Op.eq]: null }
                }
            }, { transaction })

            if (amount > depositLimit) throw new Exception('Deposit limit exceeded')

            Profile.increment('balance', { by: amount, where: { id: req.profile.id } }, { transaction })
        });

    } catch (error) {
        console.log(error)
        return res.status(500).end()
    }

    res.end()
})

/**
 * @returns Returns the profession that earned the most money (sum of jobs paid)
 * or any contactor that worked in the query time range.
 */
app.get('/admin/best-profession', getProfile, async (req, res) => {
    const { Contract, Job, Profile } = req.app.get('models')
    const { start, end } = req.query

    const job = await Job.findOne({
        attributes: [[sequelize.fn('sum', sequelize.col('price')), 'total_cost']],
        include: {
            model: Contract,
            attributes: ['contractorId'],
            include: {
                model: Profile,
                as: 'Contractor',
                where: { type: 'contractor' },
                attributes: ['profession']
            }
        },
        where: {
            paymentDate: { [Op.between]: [start, end] }
        },
        group: ['Contract.Contractor.profession'],
        order: [[sequelize.fn('sum', sequelize.col('price')), 'DESC']]
    })

    if (!job) return res.status(404).end()
    res.json(job.Contract.Contractor.profession)
})

/**
 * @returns Returns the clients the paid the most for jobs in the query time period.
 * limit query parameter should be applied, default limit is 2.
 * TODO: move fullname from Client to root
 */
app.get('/admin/best-clients', getProfile, async (req, res) => {
    const { Contract, Job, Profile } = req.app.get('models')
    const { start, end, limit } = req.query

    const bestClients = await Job.findAll({
        attributes: [
            [sequelize.literal("Contract.clientId"), "id"],
            [sequelize.fn('sum', sequelize.col('price')), 'paid'],
            // [sequelize.literal("Contract.Client.fullname"), "fullname"]
        ],
        include: {
            model: Contract,
            attributes: ['clientId'],
            include: {
                model: Profile,
                as: 'Client',
                where: { type: 'client' },
                attributes: [[sequelize.literal("firstname || ' ' || lastname"), "fullname"]]
            }
        },
        where: {
            paymentDate: { [Op.between]: [start, end] }
        },
        group: ['Contract.Client.id'],
        order: [[sequelize.fn('sum', sequelize.col('price')), 'DESC']],
        limit: limit || 2
    })

    if (!bestClients) return res.status(404).end()
    res.json(bestClients)
})

module.exports = app;
